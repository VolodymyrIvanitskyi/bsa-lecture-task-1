﻿using System;
using System.Collections.Generic;
using HomeWork1.Models;
using HomeWork1.Services;

namespace HomeWork1
{
    class Program
    {
        private static QueryService queryService = new QueryService();
        static void Main(string[] args)
        {

            while (true)
            {
                BasicMenu();

                var input = Console.ReadKey();
                Console.WriteLine();
                int id = -1;
                switch (input.KeyChar)
                {
                    case '1':
                        id = InputId();
                        if (id == -1)
                        {
                            IncorrectInput();
                            return;
                        }
                        Dictionary<int,int> result = queryService.GetCountTasksByUser(id);
                        if (result.Count > 0)
                        {
                            foreach (var item in result)
                            {
                                Console.WriteLine("Id проeкту: " + item.Key + ", кількість тасків:" + item.Value+"\n");
                            };
                        }
                        else
                        {
                            Console.WriteLine($"У працівника з Id {id} ще немає тасків\n");
                        }
                        break;
                    case '2':
                        id = InputId();
                        if (id == -1)
                        {
                            IncorrectInput();
                            return;
                        }
                        var tasks = queryService.GetTasksForUser(id);
                        foreach(var task in tasks)
                        {
                            Console.WriteLine($"Завдання: {task.Name}, Id виконавця: {task.PerformerId}");
                        }
                        Console.WriteLine("");
                        break;
                    case '3':
                        id = InputId();
                        if (id == -1)
                        {
                            IncorrectInput();
                            return;
                        }

                        var finishedTasks = queryService.GetFinishedTasksForUser(id);
                        if (finishedTasks.Count > 0)
                        {
                            foreach (var task in finishedTasks)
                            {
                                Console.WriteLine($"Id таски: {task.Id}, таска: {task.Name}");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Працівник не виконав жодної таски");
                        }
                        break;
                    case '4':
                        var teamWhenUsersOlderTenYear = queryService.GetTeamsWhenMembersOlderThan10Years();
                        if (teamWhenUsersOlderTenYear.Count > 0)
                        {
                            foreach(var team in teamWhenUsersOlderTenYear)
                            {
                                Console.WriteLine($"\nКоманда: {team.Name}, працівники:");

                                for(int i = 0; i < team.Users.Count; i++)
                                {
                                    Console.WriteLine($"Ім'я: {team.Users[i].FirstName}, Прізвище: {team.Users[i].LastName}, Email: {team.Users[i].Email}");
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("Список відсутній");
                        }
                        break;
                    case '5':
                        var usersAlphabetically = queryService.GetUsersAlphabetically();
                        foreach (var item in usersAlphabetically)
                        {
                            Console.WriteLine($"Працівник: {item.FirstName}");
                            if (item.Tasks?.Count > 0)
                            {
                                foreach(var task in item.Tasks)
                                    Console.WriteLine("Id таски: " + task.Id + "; Ім'я таски - " + task.Name);
                            }
                            else
                            {
                                Console.WriteLine("Тасок немає");
                            }
                        }
                        break;
                    case '6':
                        id = InputId();
                        if (id == -1)
                        {
                            IncorrectInput();
                            return;
                        }
                        var data = queryService.GetDatafromUser(id);
                        if (data != null)
                        {
                            Console.WriteLine($"Працівник {data.User.FirstName},\n" +
                                $"Останній проект: {data.LastProject?.Id}\n" +
                                $"Загальна кількість тасків під останнім проектом: {data.CountOfTasks}\n" +
                                $"Загальна кількість незавершених або скасованих тасків: {data.CountOfCanceledTasks}\n" +
                                $"Найтриваліший таск користувача за датою: {data.TheLongestTask.Id}");
                        }
                        else
                        {
                            Console.WriteLine("Даних немає");
                        }


                        break;
                    case '7':
                        var dataFromProject = queryService.GetDataFromProject();
                        foreach (var item in dataFromProject)
                        {
                            Console.WriteLine($"Id проекту: {item.Project.Id}\n" +
                                $"Найдовший таск проекту (за описом): Id {item.TheShortestTask?.Id}, Name {item.TheLongestTask?.Name}\n" +
                                $"Найкоротший таск проекту (по імені): Id {item.TheShortestTask?.Id}, Name {item.TheShortestTask?.Name}\n" +
                                $"Загальна кількість користувачів в команді проекту, де або опис проекту >20 символів, або кількість тасків <3 : {item.CountOfUsers}");
                        };
                        break;
                    default:
                        IncorrectInput();
                        break;
                }
            }
        }

        private static void BasicMenu()
        {
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
            Console.WriteLine("1 - Отримати кількість тасків у проекті конкретного користувача (по id)");
            Console.WriteLine("2-Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів");
            Console.WriteLine("3-Отримати список (id, name) з колекції тасків, які виконані (finished) в поточному (2021) році для конкретного користувача ");
            Console.WriteLine("4-Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років, відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.");
            Console.WriteLine("5-Отримати список користувачів за алфавітом first_name (по зростанню) з відсортованими tasks по довжині name (за спаданням).");
            Console.WriteLine("6-Отримати інформацію про останній проект користувача,\n" +
                " загальну кількість тасків під останнім проектом,\n" +
                " загальну кількість незавершених або скасованих тасків для користувача,\n" +
                " Найтриваліший таск користувача за датою (найраніше створений - найпізніше закінчений)");
            Console.WriteLine("7-Отримати:\n" +
                "Найдовший таск проекту (за описом)\n" +
                "Найкоротший таск проекту (по імені)\n" +
                "Загальнe кількість користувачів в команді проекту, де або опис проекту >20 символів, або кількість тасків <3");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");

            Console.Write("Введіть число (1-7): ");
        }

        private static int InputId()
        {
            Console.Write("Введіть Id:");
            try
            {
                return int.Parse(Console.ReadLine());
            }
            catch { return -1; }
        }
        private static void IncorrectInput()
        {
            Console.WriteLine("Неправильні вхідні параметри!!!");
        }
    }
}
