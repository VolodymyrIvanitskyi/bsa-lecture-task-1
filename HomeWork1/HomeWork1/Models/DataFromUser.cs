﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork1.Models
{
    public class DataFromUser
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int? CountOfTasks { get; set; }
        public int? CountOfCanceledTasks { get; set; }
        public Task TheLongestTask { get; set; }
    }
}
