﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork1.Models
{
    public class DataFromProject
    {
        public Project Project { get; set; }
        public Task TheLongestTask { get; set; }
        public Task TheShortestTask { get; set; }
        public int? CountOfUsers { get; set; }

    }
}
