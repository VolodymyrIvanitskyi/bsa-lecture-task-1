﻿namespace HomeWork1.Models
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
