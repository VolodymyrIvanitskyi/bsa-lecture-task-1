﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeWork1.Services;
using Newtonsoft.Json;
using System.Net.Http;
using HomeWork1.Models;

namespace HomeWork1.Services
{
    public class QueryService
    {
        private static readonly HttpService _httpService;

        private List<Project> _projects;
        private List<Models.Task> _tasks;
        private List<User> _users;
        private List<Team> _teams;

        static QueryService()
        {
            _httpService = new HttpService();
        }
        public QueryService()
        {
            InitData();
        }
        private void InitData()
        {
            _teams =  GetEntities<Team>("Teams").Result;
            _projects =  GetEntities<Project>("Projects").Result;
            _users = GetEntities<User>("Users").Result;
            _tasks =  GetEntities<Models.Task>("Tasks").Result;
        }
       

        public async Task<List<T>> GetEntities<T>(string path)
        {
            try
            {
                string responceBody = await _httpService.GetEntities(path);

                var result = JsonConvert.DeserializeObject<List<T>>(responceBody);
                return result;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            return null;


        }

        public Dictionary<int, int> GetCountTasksByUser(int authorId)
        {
            return _projects
                .Where(x => x.AuthorId == authorId)
                .GroupJoin(_tasks,
                project => project.Id,
                task => task.ProjectId,
                (project, tasks) => new {
                    projectId = project.Id,
                    countOfTasks = tasks.Count()
                })
                .ToDictionary(d => d.projectId, d => d.countOfTasks);
        }

        public List<Models.Task> GetTasksForUser(int userId)
        {
            return _tasks
                .Where(t => t.PerformerId == userId  && t.Name.Length < 45)
                .ToList();
        }

        public List<FinishedTask> GetFinishedTasksForUser(int userId)
        {
            return _tasks
                .Where(t => t.State == TaskState.Finished && t.PerformerId == userId && t.FinishedAt?.Year == 2021)
                .Select(t=> new FinishedTask{ Id = t.Id, Name = t.Name})
                .ToList();
        
        }

        public List<OlderTeam> GetTeamsWhenMembersOlderThan10Years()
        {
            
            return _teams
                .GroupJoin(_users,
                team => team.Id,
                user => user.TeamId,
                (team, users) =>
                new OlderTeam()
                {
                    Id = team.Id,
                    Name = team.Name,
                    Users = users.Where(user => (DateTime.Now.Year - user.BirthDay.Year) > 10).ToList()
                }).ToList();
        }

        public List<User> GetUsersAlphabetically()
        {
            return _users.GroupJoin(    
                _tasks,
                user => user.Id,
                task => task.PerformerId,
                (user, tasks) => {
                    user.Tasks = tasks.OrderByDescending(x => x.Name.Length).ToList();
                    return user;
                })
                .OrderBy(user => user.FirstName)
                .ToList();
        }

        public DataFromUser GetDatafromUser(int id)
        {
            return new DataFromUser()
            {
                User = _users
                    .Where(u => u.Id == id)
                    .FirstOrDefault(),

                LastProject = _projects
                    .Where(project => project?.AuthorId == id)
                    .OrderByDescending(x => x.CreatedAt)
                    .FirstOrDefault(),

                CountOfTasks = _tasks
                    .Where(task => task.ProjectId == _projects
                    .Where(project => project?.AuthorId == id)
                    .OrderByDescending(project => project.CreatedAt)
                    .FirstOrDefault()?.Id
                    ).Count(),

                CountOfCanceledTasks = _tasks
                    .Where(t => t.PerformerId == id && (t.State == TaskState.Canceled || t.State == TaskState.Started))
                    .Count(),

                TheLongestTask = _tasks
                    .Where(task => task.PerformerId == id)
                    .OrderByDescending(task => task.FinishedAt - task.CreatedAt)
                    .FirstOrDefault()
            };
        }

        public List<DataFromProject> GetDataFromProject()
        {
            var aboutProject = _teams
                .GroupJoin(
                _users,
                t => t.Id,
                u => u.TeamId,
                (team, u) =>
                {
                    team.Users = u.ToList();
                    return team;
                })
                .Join(
                _projects,
                t => t.Id,
                p => p.TeamId,
                (t, project) =>
                {
                    project.Team = t;
                    return project;
                })
                .GroupJoin(
                _tasks,
                p => p.Id,
                t => t.ProjectId,
                (project, t) =>
                {
                    project.Tasks = t.ToList();
                    return project;
                })
                .Select(
                project => new DataFromProject()
                {
                    Project = project,

                    TheLongestTask = project
                    .Tasks?
                    .OrderByDescending(t => t.Description.Length)
                    .FirstOrDefault(),
                    TheShortestTask = project
                    .Tasks?
                    .OrderBy(t => t.Name.Length)
                    .FirstOrDefault(),
                    CountOfUsers = project.Tasks.Count < 3
                    || project.Description.Length > 20
                    ? project.Team.Users?.Count : 0
                }).OrderBy(p => p.Project.Id).ToList();

            return aboutProject;

        }
    }
}
